// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// Це об'єктна модель документа, яка представляє весь вміст сторінки у вигляді об'єктів, які можна міняти. За її допомогою ми можемо щось створювати чи змінювати на сторінці.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText — показує весь текстовий вміст, який не відноситься до синтаксису HTML. Тобто будь-який текст, укладений між елементами, що відкривають і закривають, буде записаний в innerText. Причому якщо всередині innerText будуть ще якісь елементи HTML зі своїм вмістом, він проігнорує самі елементи і поверне їх внутрішній текст.
// innerHTML - Покаже текстову інформацію рівно по одному елементу. У висновок потрапить і текст і розмітка HTML-документа, яка може бути укладена між тегами основного елемента, що відкривають і закривають.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Існує 6 основних методів пошуку елементів у DOM:
// 1. querySelector
// 2. querySelectorAll
// 3. getElementById
// 4. getElementsByName
// 5. getElementsByTagName
// 6. getElementsByClassName
// Найчастіше використовуються querySelector і querySelectorAll, але getElement(s)By* може бути корисним час від часу або знаходитися в старому коді.

let paragraphs = document.querySelectorAll("p");
for(let paragraph of paragraphs) {
  paragraph.style.backgroundColor = "#ff0000";
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

let parentElement = optionsList.parentNode;
console.log(parentElement);

let childNodes = optionsList.childNodes;
for (let node of childNodes) {
  console.log(node.nodeName, node.nodeType);
}

let mainHeaders = document.querySelectorAll(".main-header");

mainHeaders.forEach(header => {
  let nestedElements = header.querySelectorAll("*");
  nestedElements.forEach(element => {
    element.classList.add("nav-item");
    console.log(element);
  });
});

let sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach((title) => {
  title.classList.remove('section-title');
});

let testParagraph = document.querySelector(".testParagraph");
testParagraph.textContent = 'This is a paragraph';
